package com.example.buildahousefor30days.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.sp
import com.example.buildahousefor30days.R

val RussoOne = FontFamily(
    Font(R.font.russoone_regular),
)

val Typography = Typography(
    displayLarge = TextStyle(
        fontFamily = RussoOne,
        fontSize = 32.sp
    ),
    displayMedium = TextStyle(
        fontFamily = RussoOne,
        fontSize = 22.sp
    ),
    labelSmall = TextStyle(
        fontFamily = RussoOne,
        fontSize = 12.sp
    ),
    bodyLarge = TextStyle(
        fontFamily = RussoOne,
        fontSize = 16.sp
    )
)