package com.example.buildahousefor30days.model

import com.example.buildahousefor30days.R

object HouseRepository {
    val houseTips = listOf(
        HouseTip(
            nameRes = R.string.tip_title_1,
            instructionRes = R.string.tip_description_1,
            imageRes = R.drawable.image_tip_1,
        ),
        HouseTip(
            nameRes = R.string.tip_title_2,
            instructionRes = R.string.tip_description_2,
            imageRes = R.drawable.image_tip_2,
        ),
        HouseTip(
            nameRes = R.string.tip_title_3,
            instructionRes = R.string.tip_description_3,
            imageRes = R.drawable.image_tip_3,
        ),
        HouseTip(
            nameRes = R.string.tip_title_5,
            instructionRes = R.string.tip_description_5,
            imageRes = R.drawable.image_tip_5,
        ),
        HouseTip(
            nameRes = R.string.tip_title_6,
            instructionRes = R.string.tip_description_6,
            imageRes = R.drawable.image_tip_6,
        ),
        HouseTip(
            nameRes = R.string.tip_title_7,
            instructionRes = R.string.tip_description_7,
            imageRes = R.drawable.image_tip_7,
        ),
        HouseTip(
            nameRes = R.string.tip_title_8,
            instructionRes = R.string.tip_description_8,
            imageRes = R.drawable.image_tip_8,
        ),HouseTip(
            nameRes = R.string.tip_title_9,
            instructionRes = R.string.tip_description_9,
            imageRes = R.drawable.image_tip_9,
        ),
        HouseTip(
            nameRes = R.string.tip_title_10,
            instructionRes = R.string.tip_description_10,
            imageRes = R.drawable.image_tip_10,
        ),HouseTip(
            nameRes = R.string.tip_title_11,
            instructionRes = R.string.tip_description_11,
            imageRes = R.drawable.image_tip_11,
        ),HouseTip(
            nameRes = R.string.tip_title_12,
            instructionRes = R.string.tip_description_12,
            imageRes = R.drawable.image_tip_12,
        ),
        HouseTip(
            nameRes = R.string.tip_title_13,
            instructionRes = R.string.tip_description_13,
            imageRes = R.drawable.image_tip_13,
        ),HouseTip(
            nameRes = R.string.tip_title_14,
            instructionRes = R.string.tip_description_14,
            imageRes = R.drawable.image_tip_14,
        ),
        HouseTip(
            nameRes = R.string.tip_title_15,
            instructionRes = R.string.tip_description_15,
            imageRes = R.drawable.image_tip_15,
        ),
        HouseTip(
            nameRes = R.string.tip_title_16,
            instructionRes = R.string.tip_description_16,
            imageRes = R.drawable.image_tip_16,
        ),
        HouseTip(
            nameRes = R.string.tip_title_17,
            instructionRes = R.string.tip_description_17,
            imageRes = R.drawable.image_tip_17,
        ),
        HouseTip(
            nameRes = R.string.tip_title_18,
            instructionRes = R.string.tip_description_18,
            imageRes = R.drawable.image_tip_18,
        ),
        HouseTip(
            nameRes = R.string.tip_title_19,
            instructionRes = R.string.tip_description_19,
            imageRes = R.drawable.image_tip_19,
        ),
        HouseTip(
            nameRes = R.string.tip_title_19,
            instructionRes = R.string.tip_description_19,
            imageRes = R.drawable.image_tip_20,
        ),
        HouseTip(
            nameRes = R.string.tip_title_21,
            instructionRes = R.string.tip_description_21,
            imageRes = R.drawable.image_tip_21,
        ),
        HouseTip(
            nameRes = R.string.tip_title_22,
            instructionRes = R.string.tip_description_22,
            imageRes = R.drawable.image_tip_22,
        ),
        HouseTip(
            nameRes = R.string.tip_title_23,
            instructionRes = R.string.tip_description_23,
            imageRes = R.drawable.image_tip_23,
        ),
        HouseTip(
            nameRes = R.string.tip_title_24,
            instructionRes = R.string.tip_description_24,
            imageRes = R.drawable.image_tip_24,
        ),
        HouseTip(
            nameRes = R.string.tip_title_25,
            instructionRes = R.string.tip_description_25,
            imageRes = R.drawable.image_tip_25,
        ),
        HouseTip(
            nameRes = R.string.tip_title_26,
            instructionRes = R.string.tip_description_26,
            imageRes = R.drawable.image_tip_26,
        ),
        HouseTip(
            nameRes = R.string.tip_title_27,
            instructionRes = R.string.tip_description_27,
            imageRes = R.drawable.image_tip_27,
        ),
        HouseTip(
            nameRes = R.string.tip_title_28,
            instructionRes = R.string.tip_description_28,
            imageRes = R.drawable.image_tip_28,
        ),
        HouseTip(
            nameRes = R.string.tip_title_29,
            instructionRes = R.string.tip_description_29,
            imageRes = R.drawable.image_tip_29,
        ),
        HouseTip(
            nameRes = R.string.tip_title_30,
            instructionRes = R.string.tip_description_30,
            imageRes = R.drawable.image_tip_30,
        )
    )
}